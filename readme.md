# Ham Galaxy Settings

This mod adds a new config file to your galaxy, wich gives you access to a lot of new custom settings to modify balancing and difficulty of the whole galaxy. The goal is to make the game more individual for the player / server, and to make you able to design it much more interesting and challanging without modding by your own.

For example you can make npc ships or stations stronger, make npc stations have more cargo space (a mine wich can only store 100 Gold is annoying), modify the global damage of military-, civil weapons and torpedos or nerf faction wars. Also you can modify the scaling of difficulty when you get nearer to core.

Watch the [list of all settings](https://gitlab.com/hams-avorion-projects/ham-galaxy-settings/wikis/settings-and-how-to-change-it) for more information.

By default this mod provides a setting, that does not change the balancing at all. I want the user himself decide about this.



### Installation and documentation

* Subscribe to this mod and Steam and activate for your galaxy.
* Watch [Documentation](https://gitlab.com/hams-avorion-projects/ham-galaxy-settings/wikis/Home)


