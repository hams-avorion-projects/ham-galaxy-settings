package.path = package.path .. ";data/scripts/lib/?.lua"

local HGSLib = include("HGSLib")

local HGSGenerateMiningLaser = WeaponGenerator.generateMiningLaser
function WeaponGenerator.generateMiningLaser(rand, dps, tech, material, rarity)
    local weapon = HGSGenerateMiningLaser(rand, dps, tech, material, rarity)
	
	weapon.reach = weapon.reach * HGSLib.get("globalDisassemblyRange")
	weapon.blength = weapon.reach * HGSLib.get("globalDisassemblyRange")

    return weapon
end

local HGSGenerateSalvagingLaser = WeaponGenerator.generateSalvagingLaser
function WeaponGenerator.generateSalvagingLaser(rand, dps, tech, material, rarity)
    local weapon = HGSGenerateSalvagingLaser(rand, dps, tech, material, rarity)

	weapon.reach = weapon.reach * HGSLib.get("globalDisassemblyRange")
	weapon.blength = weapon.reach * HGSLib.get("globalDisassemblyRange")
	
    return weapon
end

local HGSGenerateRawMiningLaser = WeaponGenerator.generateRawMiningLaser
function WeaponGenerator.generateRawMiningLaser(rand, dps, tech, material, rarity)
    local weapon = HGSGenerateRawMiningLaser(rand, dps, tech, material, rarity)

	weapon.reach = weapon.reach * HGSLib.get("globalDisassemblyRange")
	weapon.blength = weapon.reach * HGSLib.get("globalDisassemblyRange")
	
    return weapon
end

local HGSGenerateRawSalvagingLaser = WeaponGenerator.generateRawSalvagingLaser
function WeaponGenerator.generateRawSalvagingLaser(rand, dps, tech, material, rarity)
    local weapon = HGSGenerateRawSalvagingLaser(rand, dps, tech, material, rarity)

	weapon.reach = weapon.reach * HGSLib.get("globalDisassemblyRange")
	weapon.blength = weapon.reach * HGSLib.get("globalDisassemblyRange")
	
    return weapon
end

