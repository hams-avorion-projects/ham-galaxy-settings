package.path = package.path .. ";data/scripts/lib/?.lua"
package.path = package.path .. ";data/scripts/entity/?.lua"

local HGSLib = include("HGSLib")

local armedStationScripts = {
	"equipmentdock.lua",
	"militaryoutpost.lua",
}


SectorGenerator.HGSLibCreateStation = SectorGenerator.createStation
function SectorGenerator:createStation(faction, scriptPath, ...)
	local station = self:HGSLibCreateStation(faction, scriptPath, ...)
	
	if HGSLib.get("stationUseWeapons") then
		if not scriptPath or scriptPath == "" or not self:isArmedStation(scriptPath) then -- Dont add weapons twice
			ShipUtility.addArmedTurretsToCraft(station)
		end
	end
	
	if HGSLib.get("stationUseAntiTorp") then
		ShipUtility.addAntiTorpedoEquipment(station)
	end

    return station
end


function SectorGenerator:isArmedStation(script)
	for _,armedScript in pairs(armedStationScripts) do
		if string.find(script, armedScript) then
			return true
		end
	end
	
	return false
end