package.path = package.path .. ";data/scripts/lib/?.lua"

local HGSLib = include("HGSLib")


-- Apply coax setting
-- Removed backwards compatibility for Avorion 1.2.* or lower!
local HGSScale = TurretGenerator.scale
function TurretGenerator.scale(rand, turret, type, tech, turnSpeedFactor, coaxialPossible, ...)
	if HGSLib.get("globalNoCoax") then
		coaxialPossible = false
	end
	HGSScale(rand, turret, type, tech, turnSpeedFactor, coaxialPossible, ...)
end


