package.path = package.path .. ";data/scripts/lib/?.lua"



local HGSGenerate = TorpedoGenerator.generate
function TorpedoGenerator:generate(x, y, offset_in, rarity_in, warhead_in, body_in) -- server
	local torpedo = HGSGenerate(self, x, y, offset_in, rarity_in, warhead_in, body_in)
	
	local offset = offset_in or 0
    local sector = math.floor(length(vec2(x, y))) + offset
	
	local dps, tech = Balancing_GetSectorTorpedoDPS(sector, 0)
    dps = dps * Balancing_GetSectorTurretsUnrounded(sector, 0) -- remove turret bias
	
    local rarity = torpedo.rarity


    -- warhead properties
    local damage = dps * (1 + rarity.value * 0.25) * 10
	local warhead = Warheads[torpedo.type]
	
	torpedo.shieldDamage = round(damage * warhead.shield / 100) * 100
    torpedo.hullDamage = round(damage * warhead.hull / 100) * 100
	
	if warhead.damageVelocityFactor then
        -- scale to normal dps damage dependent on maxVelocity
        torpedo.damageVelocityFactor = damage * warhead.hull / torpedo.maxVelocity
        torpedo.hullDamage = 0
    end

    return torpedo
end


