package.path = package.path .. ";data/scripts/lib/?.lua"
package.path = package.path .. ";data/scripts/?.lua"

local HGSLib = include("HGSLib")


function Xsotan.applyCenterBuff(craft)
	local scaleFactor = HGSLib.get("coreScaleXsotans")
	
	if scaleFactor and scaleFactor > 0 then
		local sector = Sector()
		if not sector then
			print ("no sector")
			return
		end

		local x, y = sector:getCoordinates()
		if not Balancing_InsideRing(x, y) then return end
		if Galaxy():sectorInRift(x, y) then return end

		local min = Balancing_GetBlockRingMin()
		local distance = length(vec2(x, y))
		local factor = 1 - (distance / min) -- from 0 (ring) to 1 (center)
		factor = math.min(1.0, math.max(0.0, factor)) -- clamp to between 0 and 1 just to be sure

		factor = factor * scaleFactor															-- HGS modified - apply setting

		local upscale = 1.25 + (factor * 1.25) -- from 1.25 (ring) to 2.5 (center)

		craft.damageMultiplier = upscale * craft.damageMultiplier 								-- HGS modified - do not discard old damageMultiplier of the ship
	end
end