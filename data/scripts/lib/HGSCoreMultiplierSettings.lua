return {
	npcShipVolume = true,
	npcShipDurability = true,
	npcShipShields = true,
	npcStationVolume = true,
	npcStationDurability = true,
	npcStationShields = true,
	npcDmgMulti = true,
	globalWeaponDps = true,
	globalMiningDps = true,
	globalSalvagingDps = true,
	globalTorpDps = true,
}