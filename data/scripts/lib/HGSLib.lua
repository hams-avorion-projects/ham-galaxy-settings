package.path = package.path .. ";data/scripts/lib/?.lua"
package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"

local HGSLib = {}
local ConfigLib = include("ConfigLib")
local HGSConfigLib = ConfigLib("1765064717")

local coreMultiSettings = include("HGSCoreMultiplierSettings")
--local coreSettingCalc

include("utility")
include("extutils")


-- Load a value from the config lib
function HGSLib.get(var, x, y)
	local coreMulti
	if coreMultiSettings[var] and x and y then
		local dist = length(vec2(x, y))
		strCoreSettingCalc = "local d = " .. dist .. " local cd = " .. HGSConfigLib.get("coreSettingDistance") .. " return " .. HGSConfigLib.get("coreSettingCalc")
		
		coreMulti = loadstring(strCoreSettingCalc)() * (HGSConfigLib.get(var .. "Core") -1) +1
		coreMulti = math.max(coreMulti or 1, 1)
		
		return HGSConfigLib.get(var) * coreMulti
	end
	
	-- Not a ...core value
	return HGSConfigLib.get(var)
end


function HGSLib.log(...)
	HGSConfigLib.log(...)
end


return HGSLib


