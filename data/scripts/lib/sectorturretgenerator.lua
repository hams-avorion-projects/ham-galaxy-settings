package.path = package.path .. ";data/scripts/lib/?.lua"

local HGSLib = include("HGSLib")

-- Copy pasted from vanilla
-- Added maxRarity property, 1.3.0
function SectorTurretGenerator:generate(x, y, offset_in, rarity_in, type_in, material_in)

    local offset = offset_in or 0
    local dps = 0

    local rarities = self.rarities or self:getSectorRarityDistribution(x, y)
    local rarity = rarity_in or Rarity(getValueFromDistribution(rarities, self.random))
	
    if self.minRarity then
        if rarity < self.minRarity then rarity = self.minRarity end
    end
	if self.maxRarity then -- Added maxRarity property, 1.3.0
		if rarity > self.maxRarity then rarity = self.maxRarity end
	end
	
    local seed, qx, qy = self:getTurretSeed(x, y, weaponType, rarity)

    local sector = math.max(0, math.floor(length(vec2(qx, qy))) + offset)

    local weaponDPS, weaponTech = Balancing_GetSectorWeaponDPS(sector, 0)
    local miningDPS, miningTech = Balancing_GetSectorMiningDPS(sector, 0)
    local materialProbabilities = Balancing_GetTechnologyMaterialProbability(sector, 0)
    local material = material_in or Material(getValueFromDistribution(materialProbabilities, self.random))
    local weaponType = type_in or getValueFromDistribution(Balancing_GetWeaponProbability(sector, 0), self.random)
	
	local salvagingDPS, salvagingTech = Balancing_GetSectorSalvagingDPS(sector, 0)								-- HGS Modified

    local tech = 0
    if weaponType == WeaponType.MiningLaser then
        dps = miningDPS
        tech = miningTech
    elseif weaponType == WeaponType.RawMiningLaser then
		local multi = 1.6
        if GameVersion() < Version(2, 0, 0) then -- Backwards compatibility for Avorion 1.*
			multi = 2
		end
		HGSLib.log(2, "MULTI", multi)
		dps = miningDPS * multi
        tech = miningTech
	elseif weaponType == WeaponType.SalvagingLaser or weaponType == WeaponType.RawSalvagingLaser then 			-- HGS Modified
		dps = salvagingDPS
        tech = salvagingTech
    elseif weaponType == WeaponType.ForceGun then
        dps = 1200
        tech = weaponTech
    else
        dps = weaponDPS
        tech = weaponTech
    end

    return TurretGenerator.generateSeeded(seed, weaponType, dps, tech, rarity, material)
end

