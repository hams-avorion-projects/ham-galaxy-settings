package.path = package.path .. ";data/scripts/lib/?.lua"

local HGSLib = include("HGSLib")

local blockRingMax = 150; -- Yay I finally can mod this via config :)
local blockRingMin = blockRingMax - 3;

local HGSLibBalancing_GetSectorShipVolume = Balancing_GetSectorShipVolume
function Balancing_GetSectorShipVolume(x, y, orig)
	local multi = 1
	if not orig then
		multi = HGSLib.get("npcShipVolume", x, y)
	end
	
	return HGSLibBalancing_GetSectorShipVolume(x, y) * multi
end



function Balancing_GetSectorStationVolume(x, y)
	return Balancing_GetSectorShipVolume(x, y, true) * 100 * HGSLib.get("npcStationVolume", x, y) -- Remove max station size, dont understand a cap of 150k....
end



function Balancing_GetSectorShipHP(x, y)
    -- an average craft in this sector has approx. this health
    -- blocks have an average durability of volume * 4
    local materialStrength = Balancing_GetSectorMaterialStrength(x, y)
    local shipVolume = Balancing_GetSectorShipVolume(x, y, true)

    return shipVolume * materialStrength * 4.0
end


local HGSLibBalancing_GetSectorWeaponDPS = Balancing_GetSectorWeaponDPS
function Balancing_GetSectorWeaponDPS(x, y, vanilla)
	local dps, tech = HGSLibBalancing_GetSectorWeaponDPS(x, y)
	
	if not vanilla then
		dps = dps * HGSLib.get("globalWeaponDps", x, y)
	end
	
	return dps, tech
end


function Balancing_GetSectorSalvagingDPS(x, y)
	local dps, tech = Balancing_GetSectorWeaponDPS(x, y, true)
	
	dps = dps * HGSLib.get("globalSalvagingDps", x, y)
	
	return dps, tech
end


local HGSLibBalancing_GetSectorMiningDPS = Balancing_GetSectorMiningDPS
function Balancing_GetSectorMiningDPS(x, y, vanilla)
	local dps, tech = HGSLibBalancing_GetSectorMiningDPS(x, y)
	
	if not vanilla then
		dps = dps * HGSLib.get("globalMiningDps", x, y)
	end
	
	return dps, tech
end

function Balancing_GetSectorTorpedoDPS(x, y)
	local dps, tech = Balancing_GetSectorWeaponDPS(x, y, true)
	
	dps = dps * HGSLib.get("globalTorpDps", x, y)
	
	return dps, tech
end


