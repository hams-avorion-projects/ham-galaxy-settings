package.path = package.path .. ";data/scripts/lib/?.lua"
package.path = package.path .. ";data/scripts/?.lua"

local HGSLib = include("HGSLib")

local HGSLibAddTurretsToCraft = ShipUtility.addTurretsToCraft
function ShipUtility.addTurretsToCraft(entity, turret, numTurrets, maxNumTurrets, skipDmgMulti)
	HGSLibAddTurretsToCraft(entity, turret, numTurrets, maxNumTurrets)
	
	-- Apply npc damage multiplier
	if not skipDmgMulti and not entity:getValue("hgsDmgMultiApplied") then
		local x, y = Sector():getCoordinates()
		
		entity.damageMultiplier = entity.damageMultiplier * HGSLib.get("npcDmgMulti", x, y)
		
		entity:setValue("hgsDmgMultiApplied", 1)
	end
end