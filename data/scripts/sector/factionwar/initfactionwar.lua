if onServer() then

package.path = package.path .. ";data/scripts/lib/?.lua"

local HGSLib = include("HGSLib")

local HGSLibTryStartBattle = InitFactionWar.tryStartBattle
function InitFactionWar.tryStartBattle()
	local chance = HGSLib.get("fwInitChance")
	local start = true
	local rand = 0
	
	if chance < 100 and chance > 0 then
		rand = math.random() * 100
		if rand >= chance then
			start = false
		end
	end
	
	HGSLib.log(4, "Init faction war - chance:", chance, "random:", rand, "start:", start)
	if start and chance > 0 then
		HGSLibTryStartBattle()
	end
end



end