package.path = package.path .. ";data/scripts/lib/?.lua"

include("galaxy")

local HGSLib = include("HGSLib")
local initialized

local distMulti


local tradingScripts = {
	"data/scripts/entity/merchants/factory.lua",
	"data/scripts/entity/merchants/basefactory.lua",
	"data/scripts/entity/merchants/lowfactory.lua",
	"data/scripts/entity/merchants/midfactory.lua",
	"data/scripts/entity/merchants/highfactory.lua",
	"data/scripts/entity/merchants/consumer.lua",
	"data/scripts/entity/merchants/tradingpost.lua",
	"data/scripts/entity/merchants/planetarytradingpost.lua",
	"data/scripts/entity/merchants/seller.lua",
	"data/scripts/entity/merchants/turretfactoryseller.lua",
}

-- Don't remove or alter the following comment, it tells the game the namespace this script lives in. If you remove it, the script will break.
-- namespace HGSNpcEntity
HGSNpcEntity = {}


function HGSNpcEntity.initialize()
	local entity = Entity()
	
	-- Reset some data this mod may have modified, when it is not AI owned
	-- Then terminate script
	-- Not sure if this can happen though...
	if not entity.aiOwned then
		HGSLib.log(1, "Entity had HGSNpcEntity.lua script running, but was not AI owned:")
		HGSLib.log(1, "Id:", entity.id)
		HGSLib.log(1, "Name:", entity.name)
		HGSLib.log(1, "Type:", entity.typename)
		HGSNpcEntity.terminate()
		return
	end
	
	entity:registerCallback("onBoardingSuccessful", "terminate")
	
	
	local hullMulti = 1
	local shieldMulti = 1
	
	local x, y = Sector():getCoordinates()
	
	-- Stations
	if entity.isStation then
		hullMulti = math.max(1, HGSLib.get("npcStationDurability", x, y) or 1)
		shieldMulti = math.max(1, HGSLib.get("npcStationShields", x, y) or 1)
	end
	
	-- Ships
	if entity.isShip then
		hullMulti = math.max(1, HGSLib.get("npcShipDurability", x, y) or 1)
		shieldMulti = math.max(1, HGSLib.get("npcShipShields", x, y) or 1)
	end
	
	local durability = Durability()
	durability.maxDurabilityFactor = hullMulti
	
	-- Using mod ID as prefix for bonus ID
	entity:addKeyedMultiplier(StatsBonuses.ShieldDurability, 17650647171, shieldMulti)
end


-- Fake delayed initialize() by executing updateServer() using high update interval
-- Trading scripts has to be initialized before this
-- ToDo: Implement initializationFinished() instead
function HGSNpcEntity.updateServer()
	local entity = Entity()
	
	-- Fake delayed function HGSNpcEntity.initialize() start
	if not initialized then
		
		-- Initialize station
		if entity.isStation then
			HGSNpcEntity.initializeStation()
		end
		
	end
	-- HGSNpcEntity.initialize() end
end


function HGSNpcEntity.getUpdateInterval()
	return 4
end



function HGSNpcEntity.terminate()
	local entity = Entity()
	local cargoBay = CargoBay()
	cargoBay.fixedSize = false
	
	local durability = Durability()
	durability.maxDurabilityFactor = 1
	
	entity:removeBonus(17650647171)
	
	entity.damageMultiplier = 1
	
	terminate()
end

------------------------------------
-- NPC Station START ---------------
------------------------------------
function HGSNpcEntity.initializeStation()
	local entity = Entity()
	HGSLib.log(3, "Process station cargo:", entity.title, entity.name)
	
	if HGSLib.get("stationMinCargoPerGood") > 0 then
		local minStock, slots = HGSNpcEntity.getMinStock()
		if minStock and entity.maxCargoSpace < minStock then
			if HGSLib.get("stationMaxCargoPerGood") > 0 then
				local diff = (HGSLib.get("stationMaxCargoPerGood") - HGSLib.get("stationMinCargoPerGood")) * HGSNpcEntity.getDistMulti()
				local randomize = math.random() * diff
				minStock = minStock + (randomize * slots)
			end
			
			local cargoBay = CargoBay()
			cargoBay.fixedSize = true
			cargoBay.cargoHold = minStock
			
			HGSLib.log(3, "Set fixed cargo space to station:", minStock)
		else
			HGSLib.log(4, "Skip station. Min stock:", minStock, "  Entity cargo:", entity.maxCargoSpace)
		end
	end
	initialized = true
	
	HGSNpcEntity.updateServer = nil
	HGSNpcEntity.getUpdateInterval = nil
end


function HGSNpcEntity.getMinStock()
	local entity = Entity()
	local script = HGSNpcEntity.getTradingScript()
	if not script then return 0 end
	
	local minStock = 0
	
	
	local slots = 0
	
	local soldGoods = { entity:invokeFunction(script, "getSoldGoods") }
	if soldGoods[2] then
		slots = slots + (#soldGoods - 1)
	end
	
	local boughtGoods = { entity:invokeFunction(script, "getBoughtGoods") }
	if boughtGoods[2] then
		slots = slots + (#boughtGoods - 1)
	end
	
	if slots then
		minStock = HGSLib.get("stationMinCargoPerGood") * slots * HGSNpcEntity.getDistMulti()
		minStock = minStock + 99 * slots -- Trading manager floors to 100ish steps
	end
	
	HGSLib.log(3, "distMulti:", HGSNpcEntity.getDistMulti())
	HGSLib.log(3, "Slots:", slots)
	HGSLib.log(3, "Min stock:", minStock)
	
	return minStock, slots
end

function HGSNpcEntity.getDistMulti()
	if not distMulti then
		distMulti = 1
		if HGSLib.get("stationCargoLinear") then
			local coords = vec2(Sector():getCoordinates())

			local maxDist = Balancing_GetDimensions() / 2
			local dist = length(coords)
			
			distMulti = 1.0 - math.min(1, (dist / maxDist)) -- range 0 to 1, 0 at the very edge
		end
	end
	
	return distMulti
end

function HGSNpcEntity.getTradingScript()
	local entity = Entity()
	for _,script in ipairs(tradingScripts) do
		if entity:hasScript(script) then
			return script
		end
	end
	HGSLib.log(4, "Could not determine trading script.")
	return false
end
------------------------------------
-- NPC Station STOP ----------------
------------------------------------





