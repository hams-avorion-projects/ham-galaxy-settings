# Changelog
### 1.3.0
* Updated mod for Avorion 2.*
* Removed backwards compatibility for Avorion 1.2.* or lower
* Removed config `fwNoFighterOnAttacker` because vanilla npc fighter handling improved a lot

### 1.2.2
* Prepared mod for Avorion 1.3

### 1.2.1
* Hotfix

### 1.2.0
* Moved npcStation.lua to HGSNpcEntity.lua and reworked it - keeping backwards compatibility
* Fixed a possible exploit where you have cargo bonuses still active after successful boarding a station

Added new settings to the mod:

* You can now apply stationMinCargoPerGood linearly dependant to distance from core
* Increase durability of npc stations and/or ships
* Increase shield durability of npc stations and/or ships

Note: you do not need to do anything - renamed script and new settings will update automaticly, keeping all your custom settings. After you launched your game/server with the new version for the first time, the new settings will appear in the config file in moddata folder.

@Modders: if you extended the npcStation.lua script in your own mods, this file may not work anymore, check your mod!


### 1.1.1
* Updated mod for Avorion 1.0

### 1.1.0
* Updated mod for Avorion 0.29
* Fixed an issue where weapons would still have coaxial damage bonus when coaxial weapons are disabled

### 1.0.1
* Added compatibility for Avorion 0.24.*
* Improved Xsotan upscale calculations

### 1.0.0
* Reworked mod for workshop
* Moved Wormwhole scout into a seperated mod

### 0.4.1
* Updated mod for Avorion 0.21.4

### 0.4.0
* Updated mod for Avorion 0.21.x

### 0.3.1
* Added mine corp support
* Added new setting: xsotanUseFighters - when set to false, xsotans will not use fighters anymore (performance tuning)


### 0.3.0
* Compatibility updates for Avorion 0.20.2
* Moved wormholeguardianboss.lua in a seperated namespace
* Fixed some bugs
* Improved docs and comments
* Improved logging

### 0.2.1
* Bugfixes & compatibility issues

### 0.2.0
* Removed galaxy folder usage
* Removed faulty and unfinished server-side-only implementation
* Cleaned code of mod library
* Optimized mod to be used on server (still works fine offline of course)
* Increased weapon reach for npc stations from 10km to 15km
* Fixed issues in Xsotan scaling

### 0.1.6
* Updated scripts for Avorion 0.18.3
* Changed `coreScaleXsotans` from bool to float


### 0.1.5
* New setting: `globalDisassemblyRange`
* NPC station cargo modifier uses CargoBay class now
* Improved stations weapon handling
* Bugfixing

### 0.1.4
* Minor improvements and bugfixing
* Changed wormhole guardian range setting unit to km
* Added a new setting `globalNoCoax` to disable coax weapons.

### 0.1.3
* Added or removed (not really sure wich :-P ) some minor bugs to experimental `useGalaxyPath` config, still doesnt work well. Declared it as highly experimental.

### 0.1.2
* Standardized keyed stats bonus IDs
* Minor improvements

### 0.1.1
* Improved config file handling
* Improved cargo and weapon configuration for stations

### 0.1.0
* Alpha release